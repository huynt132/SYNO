<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\DataResource;
use App\Models\Data;
use Illuminate\Http\Request;

class DataController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $result = [
            'labels' => [],
            'data' => []
        ];

        $data = DataResource::collection(Data::all());
        foreach ($data as &$datum) {
            $result['labels'][] = date('F', strtotime($datum['date']));
            $result['data'][] = $datum['value'];
        }
        return $result;
    }
}
