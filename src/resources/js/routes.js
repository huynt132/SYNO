import Dashboard from './components/Dashboard.vue';

export const routes = [
    {
        name: 'dashboard',
        path: '/',
        component: Dashboard
    }
];
