// app.js

require('./bootstrap');
import vue from 'vue'
window.Vue = vue;

import VueRouter from 'vue-router';
import {routes} from './routes';
import App from './App.vue';
import VueAxios from 'vue-axios';
import axios from 'axios';
Vue.use(VueRouter);
Vue.use(VueAxios, axios);

const router = new VueRouter({ mode: 'history', routes: routes });

const app = new Vue({
    el: '#app',
    router: router,
    render: h => h(App),
});
